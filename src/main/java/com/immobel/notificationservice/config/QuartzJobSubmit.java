package com.immobel.notificationservice.config;

import com.immobel.notificationservice.jobs.InvoiceDueNotificationJob;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

@Configuration
public class QuartzJobSubmit {

    private static final String CRON_EVERY_TWO_SECOND = "0 0/1 * ? * * *";

    @Bean(name = "notificationJobDetail")
    public JobDetailFactoryBean getNotificationJobDetail()
    {
        return QuartzConfig.createJobDetail(InvoiceDueNotificationJob.class, "notification job");
    }

    public CronTriggerFactoryBean triggerForNotificationJob(@Qualifier("notificationJobDetail") JobDetail jobDetail)
    {
        return QuartzConfig.createCronTrigger(jobDetail, CRON_EVERY_TWO_SECOND, "Notification job trigger");
    }

}
