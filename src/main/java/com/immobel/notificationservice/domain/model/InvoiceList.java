package com.immobel.notificationservice.domain.model;

import java.util.ArrayList;
import java.util.List;

public class InvoiceList {
    private List<Invoice> invoices;

    public InvoiceList()
    {
        invoices = new ArrayList<Invoice>();
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
    }
}
