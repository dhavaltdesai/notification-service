package com.immobel.notificationservice.domain.model;

import java.lang.reflect.InvocationHandler;

public class InvoiceWrapper {

    private Invoice invoice;
    private PaymentTerm paymentTerm;

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public PaymentTerm getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(PaymentTerm paymentTerm) {
        this.paymentTerm = paymentTerm;
    }
}
