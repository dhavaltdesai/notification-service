package com.immobel.notificationservice.domain.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;


@Entity
public class Invoice {

    @Id
    private String invoiceNumber;

    private LocalDate invoiceDate;


    private String paymentTermCode;

    private String invoiceStatus;

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public LocalDate getInvoiceDate() {
        return LocalDate.now();
    }

    public void setInvoiceDate(LocalDate invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getPaymentTerm() {
        return paymentTermCode;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.paymentTermCode = paymentTerm;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }



}
