package com.immobel.notificationservice.domain.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class PaymentTerm {

    @Id
    @GeneratedValue
    private Integer paymentTermId;

    private String paymentTermCode;

    private String paymentTermDescription;

    private LocalDate paymentTermCreationDate;

    private Integer paymentTermDays;

    private Integer paymentTermDaysBefore;

    public Integer getPaymentTermId() {
        return paymentTermId;
    }

    public void setPaymentTermId(Integer paymentTermId) {
        this.paymentTermId = paymentTermId;
    }

    public String getPaymentTermCode() {
        return paymentTermCode;
    }

    public void setPaymentTermCode(String paymentTermCode) {
        this.paymentTermCode = paymentTermCode;
    }

    public String getPaymentTermDescription() {
        return paymentTermDescription;
    }

    public void setPaymentTermDescription(String paymentTermDescription) {
        this.paymentTermDescription = paymentTermDescription;
    }

    public LocalDate getPaymentTermCreationDate() {
        return LocalDate.now();
    }

    public Integer getPaymentTermDays() {
        return paymentTermDays;
    }

    public void setPaymentTermDays(Integer paymentTermDays) {
        this.paymentTermDays = paymentTermDays;
    }

    public Integer getPaymentTermDaysBefore() {
        return paymentTermDaysBefore;
    }

    public void setPaymentTermDaysBefore(Integer paymentTermDaysBefore) {
        this.paymentTermDaysBefore = paymentTermDaysBefore;
    }
}