package com.immobel.notificationservice.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.immobel.notificationservice.application.NotificationAppService;

@RestController
public class NotificationServiceController {

    @Autowired
    public NotificationAppService notificationAppService;

    @GetMapping("/api/trigger")
    public ResponseEntity sendTrigger()
    {
        notificationAppService.justPrint();
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
