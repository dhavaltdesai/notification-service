package com.immobel.notificationservice.application;

import com.immobel.notificationservice.domain.model.Invoice;
import com.immobel.notificationservice.domain.model.InvoiceList;
import com.immobel.notificationservice.domain.model.InvoiceWrapper;
import com.immobel.notificationservice.domain.model.PaymentTerm;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class NotificationAppService {

    private RestTemplate rt = new RestTemplate();

    public void justPrint() {
        // get all unpaid invoices from invoice service

        Invoice inv = rt.getForObject("http://localhost:8081/api/invoice/INV-001", Invoice.class);


        ResponseEntity<Invoice[]> response = rt.getForEntity("http://localhost:8081/api/invoicequery?status=unpaid",
                Invoice[].class);

        Invoice[] invoices = response.getBody();

        List<InvoiceWrapper> wrapperList = new ArrayList<>();
        // create wrapper objects for each invoice that also has associated payment term
        for (Invoice invoice : invoices) {
            InvoiceWrapper wrapper = new InvoiceWrapper();
            wrapper.setInvoice(invoice);
            //TODO integrate with Eureka service discovery
            ResponseEntity<PaymentTerm> ptresponse = rt.getForEntity("http://localhost:8082/api/paymenttermcode?termcode=" + invoice.getPaymentTerm(), PaymentTerm.class);
            PaymentTerm pt = ptresponse.getBody();
            wrapper.setPaymentTerm(pt);
            wrapperList.add(wrapper);
        }

        processAndNotify(wrapperList);
    }

    private void processAndNotify(List<InvoiceWrapper> wrapperList)
    {
        for(InvoiceWrapper iw : wrapperList)
        {
            LocalDate invoiceCreationDate = iw.getInvoice().getInvoiceDate();
            Integer paymentTermDays = iw.getPaymentTerm().getPaymentTermDays();
            LocalDate dueDate = invoiceCreationDate.plusDays(paymentTermDays);
            Integer reminderBeforeDays = iw.getPaymentTerm().getPaymentTermDaysBefore();
            if(LocalDate.now().isEqual(dueDate.minusDays(reminderBeforeDays)))
            {
                System.out.println("##### Reminder #####");
                System.out.println("##### Payment due for invoice number : "+iw.getInvoice().getInvoiceNumber());
            }
        }
    }

}
