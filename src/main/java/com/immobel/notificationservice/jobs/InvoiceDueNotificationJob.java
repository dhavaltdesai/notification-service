package com.immobel.notificationservice.jobs;

import com.immobel.notificationservice.application.NotificationAppService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@DisallowConcurrentExecution
public class InvoiceDueNotificationJob implements Job
{
    @Autowired
    private NotificationAppService appService;

    @Override
    public void execute(JobExecutionContext context)
    {
        appService.justPrint();
    }
}
